# Final DeCharla


# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Sandra Noemy Arana Alegre
* Titulación: Ingeniería en Sistemas de Telecomunicaciones
* Cuenta en laboratorios: snarana
* Cuenta URJC: sn.arana.2019@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=e1ugaCKuIWg
* Video parte opcional (url): https://youtu.be/vRPNuTnDrt8
* Despliegue (url): http://snarana.pythonanywhere.com/    
* Contraseñas: barbie
* Cuenta Admin Site: sandra/199100ss

## Resumen parte obligatoria
DeCharla es una aplicación web que ofrece distintas salas de chat en las que los usuarios pueden publicar mensajes textuales o imágenes y votar en las salas desde la página principal. Es necesario una introducir una contraseña para acceder al sitio y ver todos los recursos. Esta autenticación puede terminarse en cualquier momento de la navegación pulsando el botón "Cerrar sesión", tras lo cual todos los mensajes volverán a aparecer como no leídos y los parámetros de configuración serán borrados.
Todos los recursos que tienen como elementos comunes son:
* Un logo del sitio que al pulsar redirige a la página principal.
* Una barra de navegación con los botones especificados en el enunciado de la práctica.
* La barra lateral extra que incluye: el nombre de charlador (por defecto 'Anonymus') y un botón "Nueva sala" que al pulsar te redirige a otra página con un formulario que permite crear salas.
* Un elemento pie que incluye el número total de: Salas activas, número de mensajes textuales y número de imágenes publicadas.
Los recursos tienen los siguientes elementos únicos:
* Página principal: Ofrece un listado de todas las salas activas permitiendo el acceso a ellas mediante click, además se indican los mensajes sin leer de cada sala, los mensajes totales publicados, botón de acceso al dinamico y al JSON.
* Página de login: Ofrece un formulario para autenticar la contraseña. Además si es incorrecta, se devuelve la página de login para volverlo a intentar.
* Página de cada sala: Ofrece un listado de los mensajes mostrando los más recientes abajo, se muestra el nombre del charlador y la fecha de publicación.
* Página para publicar mensajes mediante método PUT en formato XML: usando una aplicación externa, se pueden publicar mensajes en las salas mediante PUT.
* Página de configuración: Incluye dos formularios para establecer el nombre de charlador, el tipo y tamaño de la letra del sitio.
* Página de crear sala: Ofrece un formulario para indicar el nombre de la sala que se elija.
* Página en formato JSON: Ofrece la página de cada sala en formato JSON.
También se proporcionan los respectivos test mencionados en el enunciado.

## Lista partes opcionales

* Inclusión de un favicon del sitio: Se ha creado un favicon para caracterizar al sitio.
* Permitir que las sesiones autenticadas se puedan terminar: Mediante el botón "Cerrar sesión", ubicado en la barra de navegación. Si se usa se deberá volver a introducir la contraseña en la página de acceso (login), todos los mensajes apareceran como no leídos y los parámetros configurables se habrán reestablecido. Los votos van ligados a la cookie "session" (la misma con la que se controlan los mensajes no leídos).
* Permitir votar las salas: Se permite votar cada sala en la página principal mediante el icono del coraón, que se pondrá en rojo indicando que se ha votado, solo se podrá hacer una única vez por cada identificador de sesión.

