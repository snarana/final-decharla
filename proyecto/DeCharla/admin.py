from django.contrib import admin
from .models import Session, Passwords, Sala, Visitas, Mensajes, Megustas
# Register your models here.
admin.site.register(Session)
admin.site.register(Passwords)
admin.site.register(Sala)
admin.site.register(Visitas)
admin.site.register(Mensajes)
admin.site.register(Megustas)