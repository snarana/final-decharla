

from django.db import models
from django.utils import timezone


class Passwords(models.Model):
    password = models.TextField()


class Session(models.Model):
    session = models.TextField(unique=True)
    name = models.TextField(default="Anonymus")
    font = models.TextField()
    font_size = models.FloatField(default=1)

    def __str__(self):
        return self.name


class Sala(models.Model):
    name = models.TextField()
    creator = models.ForeignKey(Session, on_delete=models.SET_NULL, null=True)


class Visitas(models.Model):
    access_date = models.DateTimeField(default=timezone.now)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)


class Mensajes(models.Model):
    session = models.ForeignKey(Session, on_delete=models.SET_NULL, null=True)
    name = models.TextField()
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    msg = models.TextField()
    date = models.DateTimeField(default=timezone.now)
    is_img = models.BooleanField()


class Megustas(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE)
    fav = models.BooleanField(default=False)
