import json
import random
import string
import xml.etree.ElementTree as ET
from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils import timezone
from .models import Session, Passwords, Sala, Visitas, Mensajes, Megustas
from django.contrib.auth import logout


def register(request, answer):
    cookie = request.COOKIES.get("session")
    if cookie is None:
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128))
        answer.set_cookie("session", cookie)
    if request.path != "/login":
        check = Session.objects.filter(session=cookie).exists()
    else:
        check = True
    return check


def room_list():
    room_list = list(Sala.objects.all())
    return room_list


def msg_sala(request):
    msg_list = []
    cookie = request.COOKIES.get("session")
    if request.path.endswith('.json'):
        sala_name = request.path.rstrip('.json').split('/')[-1]
    else:
        sala_name = request.path.lstrip("/")
        for msg in Mensajes.objects.all():
            if msg.sala.name == sala_name:
                msg_list.append(msg)
    return msg_list


def get_total_msg(request):
    array_msgs = []
    for sala in Sala.objects.all():
        counter = 0
        for msg in Mensajes.objects.filter(sala=sala):
            counter = counter + 1
        array_msgs.append(counter)
    return array_msgs


def unread(request):
    array_counters = []
    for sala in Sala.objects.all():
        counter = 0
        cookie = request.COOKIES.get("session")
        visited = Visitas.objects.filter(sala=sala, session=Session.objects.get(session=cookie))
        if visited:
            visit = Visitas.objects.get(sala=sala, session=Session.objects.get(session=cookie))
            for msg in Mensajes.objects.filter(sala=sala):
                if visit.access_date < msg.date:
                    counter = counter+1
        else:
            counter = len(Mensajes.objects.filter(sala=sala))
        array_counters.append(counter)
    return array_counters


def username(request):
    cookie = request.COOKIES.get("session")
    sesion = Session.objects.get(session=cookie)
    name = sesion.name
    return name


def css_generator(font,font_size):
    css_content = f'''.body {{
        font-family: {font};
        font-size : {font_size}rem;
    }}'''
    return css_content


def xml(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "True":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages


def man_logout(request):
    logout(request)
    response = redirect('login')
    response.delete_cookie('session')
    return response


def footer_data(request):
    msg_count = Mensajes.objects.filter(is_img=False).count()
    img_count = Mensajes.objects.filter(is_img=True).count()
    salas_count = Sala.objects.count()
    return msg_count, img_count, salas_count


def get_fav_list(request):
    fav_list = []
    cookie = request.COOKIES.get("session")
    for fav in Megustas.objects.all():
        if fav.session.session == cookie:
            fav_list.append(fav)
    return fav_list


def check_fav(request):
    fav = []
    fav_list_chat = []
    fav_list = get_fav_list(request)
    for favour in fav_list:
        fav_list_chat.append(favour.sala)
    chat_list = room_list()
    if len(fav_list) == 0:
        for _ in chat_list:
            fav.append(False)
    else:
        for chat in chat_list:
            if chat in fav_list_chat:
                fav.append(True)
            else:
                fav.append(False)
    return fav


@ensure_csrf_cookie
def login(request):
    answer = render(request, "login.html")
    if not register(request, answer):
        return answer
    check_session = Session.objects.filter(session=request.COOKIES.get("session"))
    if check_session:
        return HttpResponseRedirect("/")
    if request.method == "POST":
        password = request.POST.get("password")
        check = Passwords.objects.filter(password=password)
        if check:
            session = Session(session=request.COOKIES.get("session"))
            session.save()
            return HttpResponseRedirect("/")
    return render(request, "login.html")


@ensure_csrf_cookie
def pageprin(request):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    msg_notread = unread(request)
    c_msg, c_img, c_salas = footer_data(request)
    total_msg = get_total_msg(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        favourite = request.POST.get("favourite")
        sala = Sala.objects.get(id=favourite)
        check_fav_chat = Megustas.objects.filter(session=session, sala=sala)
        if not check_fav_chat:
            fav_chat = Megustas(session=session, sala=sala)
            fav_chat.save()
        else:
            unfav_chat = Megustas.objects.get(session=session, sala=sala)
            unfav_chat.delete()
    favs = check_fav(request)
    datos = zip(room_list(), total_msg, msg_notread, favs)
    name = username(request)
    return render(request, "schema.html", {"room_list": room_list(), "name": name, "data_sala": datos, "session": session,
                                           "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas,
                                           "style_css": css_generator(session.font, session.font_size)})


@ensure_csrf_cookie
def room(request, sala_name):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    pagep = HttpResponseRedirect("/")
    try:
        sala = Sala.objects.get(name=sala_name)
        cookie = request.COOKIES.get("session")
        login_html = HttpResponseRedirect("/login")
        session = Session.objects.get(session=cookie)
        visited = Visitas.objects.filter(sala=sala, session=session)
        room_list = Sala.objects.all()
        if not visited:
            visit = Visitas(session=session, sala=sala)
            visit.save()
        else:
            visit = visited[0]
            visit.access_date = timezone.localtime(timezone.now())
            visit.save()
        if not register(request, login_html):
            return login_html
        if request.method == "POST":
            form = request.POST
            if "message" in form:
                msg = request.POST.get("message")
                send_value = request.POST.get("send")
                if send_value == "True":
                    is_img = True
                else:
                    is_img = False
                chat_msg = Mensajes(session=session, name=session.name, msg=msg, sala=sala, is_img=is_img)
                chat_msg.save()
        elif request.method == "PUT":
            xml_string = request.body.decode('utf-8')
            msg_xml = xml(xml_string)
            for msg in msg_xml:
                chat_msg = Mensajes(session=session, name=session.name, msg=msg['text'], sala=sala, is_img=msg['isimg'])
                chat_msg.save()

        msg_list = msg_sala(request)
        name = username(request)
        c_msg, c_img, c_salas = footer_data(request)

        return render(request, "room.html", {"msg_list": msg_list, "session": cookie, "room_list": room_list, "name": name,
                                             "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas,
                                             "style_css": css_generator(session.font, session.font_size)})
    except Sala.DoesNotExist:
        return pagep


@ensure_csrf_cookie
def newroom(request):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    c_msg, c_img, c_salas = footer_data(request)
    total_msg = get_total_msg(request)
    msg_notread = unread(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        name = request.POST.get("name")
        check_name = Sala.objects.filter(name=name)

        if not check_name.exists():
            sala = Sala(name=name, creator=session)
            sala.save()
            return HttpResponseRedirect("/" + name)
    name = username(request)
    favs = check_fav(request)
    datos = zip(room_list(), total_msg, msg_notread, favs)
    return render(request, "newroom.html", {"room_list": room_list(), "name": name, "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas,
                                            "style_css": css_generator(session.font, session.font_size), "data_sala": datos})


@ensure_csrf_cookie
def configuracion(request):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    login_html = HttpResponseRedirect("/login")
    cookie = request.COOKIES.get("session")
    total_msg = get_total_msg(request)
    msg_notread = unread(request)
    c_msg, c_img, c_salas = footer_data(request)
    name = username(request)
    if not register(request, login_html):
        return login_html
    session = Session.objects.get(session=cookie)
    if request.method == "POST":
        form = request.POST
        if "name" in form:
            name = request.POST["name"]
            session.name = name
        if "font" in form:
            font = request.POST["font"]
            session.font = font
        if "font_size" in form:
            font_size = request.POST["font_size"]
            session.font_size = float(font_size)
        session.save()
    favs = check_fav(request)
    datos = zip(room_list(), total_msg, msg_notread, favs)
    configuration_html = render(request, "configuracion.html", {"room_list": room_list(), "name": name, "style_css": css_generator(session.font, session.font_size),
                                                                "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas, "session": session, "data_sala": datos})
    return configuration_html


@ensure_csrf_cookie
def ayuda(request):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    c_msg, c_img, c_salas = footer_data(request)
    name = username(request)
    cookie = request.COOKIES.get("session")
    session = Session.objects.get(session=cookie)
    return render(request, "ayuda.html", {"name": name, "room_list": room_list(), "c_msg": c_msg, "c_img": c_img, "c_salas": c_salas,
                                          "style_css": css_generator(session.font, session.font_size)})


def get_json(request, sala_name):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    msg_list = []
    json_list = []
    cookie = request.COOKIES.get("session")
    for msg in Mensajes.objects.all():
        if msg.sala.name == sala_name:
            msg_list.append(msg)
    for msg in msg_list:
        if msg.session is None:
            msg_json = {'author': msg.name, 'msg': msg.msg, 'is_img': msg.is_img, 'date': msg.date.isoformat(), 'session': "A"}
        else:
            msg_json = {'author': msg.name, 'msg': msg.msg, 'is_img': msg.is_img, 'date': msg.date.isoformat(), 'session': msg.session.session}
        json_list.append(msg_json)
    json_therealone = json.dumps(json_list)
    return HttpResponse(json_therealone, content_type='application/json')


@ensure_csrf_cookie
def dinami(request, sala_name):
    login_html = HttpResponseRedirect("/login")
    if not register(request, login_html):
        return login_html
    home_html = HttpResponseRedirect("/")
    try:
        sala = Sala.objects.get(name=sala_name)
        cookie = request.COOKIES.get("session")
        login_html = HttpResponseRedirect("/login")
        session = Session.objects.get(session=cookie)
        if not register(request, login_html):
            return login_html
        if request.method == "POST":
            form = request.POST
            if "message" in form:
                msg = request.POST.get("message")
                send_value = request.POST.get("send")
                if send_value == "True":
                    is_img = True
                else:
                    is_img = False
                chat_msg = Mensajes(session=session, name=session.name, msg=msg, sala=sala, is_img=is_img)
                chat_msg.save()
        chat_list = room_list()
        url_json = "/" + sala_name + ".json"
        c_msg, c_img, c_salas = footer_data(request)
        name = username(request)
        return render(request, "dinami.html",
                      {"session": cookie, "chat_list": chat_list, "c_msg": c_msg, "c_img": c_img,
                       "c_salas": c_salas, "name": name, "url_json": url_json, "style_css": css_generator(session.font, session.font_size)})
    except Sala.DoesNotExist:
        return home_html
