# Generated by Django 4.1.7 on 2023-06-29 20:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0009_alter_session_font_alter_session_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='megustas',
            old_name='mg',
            new_name='fav',
        ),
    ]
