# Generated by Django 4.1.7 on 2023-06-26 23:18

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sala',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('creator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='DeCharla.session')),
            ],
        ),
        migrations.CreateModel(
            name='Visitas',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('access_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('sala', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.sala')),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.session')),
            ],
        ),
        migrations.CreateModel(
            name='Mensajes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('msg', models.TextField()),
                ('date', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_img', models.BooleanField(default=False)),
                ('sala', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DeCharla.sala')),
                ('session', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='DeCharla.session')),
            ],
        ),
    ]
