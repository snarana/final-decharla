from django.urls import path

from . import views

urlpatterns = [
    path('', views.pageprin, name='pageprin'),
    path('ayuda', views.ayuda, name='ayuda'),
    path('configuracion', views.configuracion, name='configuracion'),
    path('login', views.login, name='login'),
    path('logout', views.man_logout, name='logout'),
    path('newroom', views.newroom, name='newroom'),
    path('<str:sala_name>.json', views.get_json, name='json'),
    path('<str:sala_name>.dinami', views.dinami, name='dinami'),
    path('<str:sala_name>', views.room, name='room'),

]
