from django.test import TestCase, Client
from django.urls import reverse
from .models import Session, Passwords, Sala, Mensajes
import json


class loginTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test", name='Test')
        self.password = Passwords.objects.create(password='password')

    def test_login_unsuccessful(self):
        # GET a la vista de login
        response = self.client.get(reverse("login"))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template es el correcto
        self.assertTemplateUsed(response, "login.html")

        # POST con una contraseña incorrecta
        response = self.client.post(reverse("login"), {"password": "wrong_password"})

        # Comprobar que la respuesta es el template de login
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "login.html")

    def test_login_with_existing_session(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista de login
        response = self.client.get(reverse("login"))

        # Comprobar que la respuesta es una redirección a la página de home
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/")

class GetHomeTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test", name='Test')

    def test_get_home_authenticated(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista pageprin
        response = self.client.get(reverse("pageprin"))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template es el correcto
        self.assertTemplateUsed(response, "schema.html")

        # Comprobar que los datos están
        self.assertIn("room_list", response.context)
        self.assertIn("data_sala", response.context)
        self.assertIn("name", response.context)
        self.assertIn("c_msg", response.context)
        self.assertIn("c_img", response.context)
        self.assertIn("c_salas", response.context)
        self.assertIn("style_css", response.context)

    def test_get_home_unauthenticated(self):
        # GET a la vista pageprin sin una sesión válida
        response = self.client.get(reverse("pageprin"))

        # Comprobar que la respuesta es una redirección a la página de login
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/login")

class GetHelpTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test", name='Test')

        response = self.client.get(reverse("configuracion"))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template es el correcto
        self.assertTemplateUsed(response, "configuracion.html")

        # Comprobar que los datos esperados están presentes
        self.assertIn("room_list", response.context)
        self.assertIn("data_sala", response.context)
        self.assertIn("name", response.context)
        self.assertIn("c_msg", response.context)
        self.assertIn("c_img", response.context)
        self.assertIn("c_salas", response.context)
        self.assertIn("style_css", response.context)

class GetConfigurationViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test")

    def test_get_configuration_authenticated(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista configuracion
        response = self.client.get(reverse("configuracion"))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template es el correcto
        self.assertTemplateUsed(response, "configuracion.html")
        # Comprobar que los datos esperados están presentes
        self.assertIn("room_list", response.context)
        self.assertIn("data_sala", response.context)
        self.assertIn("name", response.context)
        self.assertIn("c_msg", response.context)
        self.assertIn("c_img", response.context)
        self.assertIn("c_salas", response.context)
        self.assertIn("style_css", response.context)

class CreateChatViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test")

    def test_create_chat_authenticated(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista newroom
        response = self.client.get(reverse("newroom"))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template es el correcto
        self.assertTemplateUsed(response, "newroom.html")

        # Comprobar que los datos esperados están presentes
        self.assertIn("room_list", response.context)
        self.assertIn("data_sala", response.context)
        self.assertIn("name", response.context)
        self.assertIn("c_msg", response.context)
        self.assertIn("c_img", response.context)
        self.assertIn("c_salas", response.context)
        self.assertIn("style_css", response.context)

class ChatTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test")
        self.sala = Sala.objects.create(name="Sala", creator=self.session)
        self.msg = Mensajes.objects.create(session=self.session, sala=self.sala, name=self.session.name,
                                               msg="Hello", is_img=False)

    def test_room_exists(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista chat
        response = self.client.get(reverse("room", args=["Sala"]))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el template utilizado está bien
        self.assertTemplateUsed(response, "room.html")

        self.assertIn("room_list", response.context)
        self.assertIn("name", response.context)
        self.assertIn("c_msg", response.context)
        self.assertIn("c_img", response.context)
        self.assertIn("c_salas", response.context)
        self.assertIn("style_css", response.context)

    def test_chat_post_message(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # POST a la vista chat para enviar un mensaje
        response = self.client.post(reverse("room", args=["Sala"]), data={"message": "Hello", "send": "False"})

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el mensaje se ha guardado en la base de datos
        self.assertTrue(
            Mensajes.objects.filter(session=self.session, sala=self.sala, msg="Hello", is_img=False).exists())


class GetJsonTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test")
        self.sala = Sala.objects.create(name="Sala", creator=self.session)
        self.sala_msg_1 = Mensajes.objects.create(session=self.session, sala=self.sala, name=self.session.name,
                                                 msg="Hello", is_img=False)
        self.sala_msg_2 = Mensajes.objects.create(session=self.session, sala=self.sala, name=self.session.name,
                                                 msg="Image", is_img=True)

    def test_get_json(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista get_json
        response = self.client.get(reverse("json", args=["Sala"]))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que el contenido de la respuesta es un JSON válido
        try:
            json_data = json.loads(response.content)
        except ValueError:
            self.fail("El contenido de la respuesta no es un JSON válido")

        # Comprobar la estructura del JSON y los datos de los mensajes
        expected_data = [
            {
                'author': self.session.name,
                'msg': 'Hello',
                'is_img': False,
                'date': self.sala_msg_1.date.isoformat(),
                'session': self.session.session
            },
            {
                'author': self.session.name,
                'msg': 'Image',
                'is_img': True,
                'date': self.sala_msg_2.date.isoformat(),
                'session': self.session.session
            }
        ]
        self.assertEqual(json_data, expected_data)

        # Comprobar que el tipo de contenido de la respuesta es 'application/json'
        self.assertEqual(response["Content-Type"], "application/json")

class DynamicChatTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session="session_test")
        self.sala = Sala.objects.create(name="Sala", creator=self.session)
        self.msg = Mensajes.objects.create(session=self.session, sala=self.sala, name=self.session.name,
                                               msg="Hello", is_img=False)

    def test_dynamic_chat(self):
        # Cookie de sesión en la solicitud
        self.client.cookies["session"] = "session_test"

        # GET a la vista dynamicChat
        response = self.client.get(reverse("dinami", args=["Sala"]))

        # Comprobar que la respuesta es un código 200
        self.assertEqual(response.status_code, 200)

        # Comprobar que la plantilla utilizada es "DynamicChat.html"
        self.assertTemplateUsed(response, "dinami.html")







